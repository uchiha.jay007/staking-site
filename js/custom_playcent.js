var vcode = "G2020"
var ckey = "X762m978b90293820m9bx873"; $(document).ready(function () { addHeader(); $('.nav-menu li a').click(function () { $(this).find('.subnav-content').toggle(); }); $('.third-button').on('click', function () { $('.animated-icon3').toggleClass('open'); }); $(".mob-toggle").click(function () { $(".nav-menu").slideToggle(); }); $('.marquee').marquee({ duration: 10000, gap: 10, delayBeforeStart: -3000, direction: 'up', duplicated: true, pauseOnHover: true, }); })
function setCookie(cname, cvalue, exdays) { var d = new Date(); d.setHours(d.getHours() + 1); var expires = "expires=" + d.toUTCString(); document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"; }
function getCookie(cname) {
  var name = cname + "="; var decodedCookie = decodeURIComponent(document.cookie); var ca = decodedCookie.split(';'); for (var i = 0; i < ca.length; i++) {
    var c = ca[i]; while (c.charAt(0) == ' ') { c = c.substring(1); }
    if (c.indexOf(name) == 0) { return c.substring(name.length, c.length); }
  }
  return "";
}
function valUser() {
  var usr = getCookie(ckey)
  if (usr != vcode) { window.location.href = "#" }
}
function login() {
  var code = document.getElementById('acode').value; if (code == '' || code == undefined) { alert('please enter code.'); return; }
  else {
    setCookie(ckey, code, 1)
    var usr = getCookie(ckey)
    if (usr == vcode) { window.location.href = "/index.html" }
    else { alert('please enter valid code.'); }
  }
}
function addHeader() {
  var header = `<div class="mob-toggle">
<a href="#" target="_blank" class="login-user mob-view">
    <img src="images/user-login.svg" alt="">
  </a>
<button class="navbar-toggler third-button" type="button" data-toggle="collapse" data-target="#toggle-expandable"
  aria-controls="toggle-expandable" aria-expanded="false" aria-label="Toggle navigation">
  <div class="animated-icon3"><span></span><span></span><span></span></div>
</button>
</div>
<div class="container">
<div class="row">
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 logo">
        <a href="index.html">
            <img src="images/playcent.png" alt="gather3r Logo" class="img-fluid">
        </a>
    </div>
    <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
        <ul class="nav-menu  float-right">
            <li class="p-2 flex-fill"><a href="#home">Home</a></li>
            <li class="p-2 flex-fill"><a href="#rewards">Rewards</a></li>
            <li class="p-2 flex-fill" ><a href="#contact">Contact</a></li>
            <li class="desk-view" style="margin-left: 0px;">
  <div class="multi-lan">
    <div id="google_translate_element"></div>
    <script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({ pageLanguage: 'en' }, 'google_translate_element');
      }
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  </div>
</li>
        </ul>
    </div>
</div>
</div>`
  jQuery('header').html(header);
}